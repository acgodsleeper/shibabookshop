<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class My_cal_discount_lib 
{
        protected $CI;
        protected $discount;
        protected $list_book_for_dc;

        public function __construct()
        {
                $this->CI =& get_instance();
                $this->CI->load->model('product_model');
                
                $this->discount = 0;
                $this->list_book_for_dc = $this->get_list_book_promotion(1);
        }

        public function cal_discount($product_arr)
        {
                $this->discount = $this->clear_discount();
                $count_harry_book = $this->check_book_for_discount($product_arr);
                
                while($count_harry_book > 0)
                {
                        $sub_discount = 0;
                        $sub_total = 0;
                        $count_unique_book = 0;
                        $percent_discount = 0;
                        $product_temp = null;
                        foreach($product_arr as $key => $row_prodect)
                        {
                                $product_temp[$key] = $row_prodect;
                                if(in_array($row_prodect['isbn'],$this->list_book_for_dc) && $row_prodect['qty']>0)
                                {
                                        $count_unique_book++;
                                        $product_temp[$key]['qty'] = $row_prodect['qty']-1;
                                        if($product_temp[$key]['qty'] == 0)
                                                $product_temp[$key] = null;
                                        $sub_total += $row_prodect['price'];
                                }
                        }

                        switch($count_unique_book)
                        {
                                case 2 : $percent_discount = 10;
                                        break;
                                case 3 : $percent_discount = 11;
                                        break;
                                case 4 : $percent_discount = 12;
                                        break;
                                case 5 : $percent_discount = 13;
                                        break;
                                case 6 : $percent_discount = 14;
                                        break;
                                case 7 : $percent_discount = 15;
                                        break;
                        }

                        $product_arr = $product_temp;
                        $count_harry_book = $this->check_book_for_discount($product_arr);
                        $sub_discount = $sub_total*$percent_discount/100;
                        $this->discount += $sub_discount;
                        
                }

                return  $this->discount;
        }

        public function check_book_for_discount($product_arr)
        {
                $count_harry_book=0;
                foreach($this->list_book_for_dc as $row_isbn)
                {
                        $found_key = array_search($row_isbn, array_column($product_arr, 'isbn'));
                        if(!empty($found_key))
                        {
                                 $count_harry_book++;
                        }
                }
                return $count_harry_book;
        }

        public function clear_discount()
        {
                return $this->discount = 0;
        }

        public function get_list_book_promotion($category_id)
        {
                $product_discount_list = $this->CI->product_model->get_product_by_category_id($category_id);
                
                $product_discount = array();
                foreach($product_discount_list->result() as $row)
                {
                        array_push($product_discount,  $row->isbn); 
                }

                return $product_discount;
        }

}
?>