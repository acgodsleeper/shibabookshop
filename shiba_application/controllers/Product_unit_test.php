<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_unit_test extends CI_Controller 
{
    public function __construct()
	{
		parent::__construct();
        $this->load->library('unit_test'); 
        $this->load->library('my_cal_discount_lib');
	}
    
    function index()
    {
        $this->load->model('product_model');

        $this->unit->run($this->product_model->get_product(), 'is_object', 'Test_get_all_product');

        $this->unit->run($this->product_model->get_product('1'), 'is_object', 'Test_get_by_product_id');

        $this->unit->run($this->product_model->get_product('AA'), 'is_object', 'Test_get_all_product');
        // where $this->test_model->func_name('22') is the test data ;
        // is_array : expected datatype
        // testname : name to your test

        $arr = Array
                (
                    0 => Array
                        (
                            'id' => 1,
                            'name' => 'Harry Potter and the Philosophers Stone  I' ,
                            'price' => 350,
                            'isbn' => '9781408855652',
                            'qty' => 2,
                            'rowid' => 'c4ca4238a0b923820dcc509a6f75849b',
                            'subtotal' => 700
                        ),
                
                    1 => Array
                        (
                            'id' => 2,
                            'name' => 'Harry Potter and the Chamber of Secrets  II' ,
                            'price' => 350,
                            'isbn' => '9781408855669',
                            'qty' => 1,
                            'rowid' => 'c81e728d9d4c2f636f067f89cc14862c',
                            'subtotal' => 350
                        ),
                
                    2 => Array
                        (
                            'id' => 9,
                            'name' => 'The Fork  the Witch  and the Worm',
                            'price' => 260,
                            'isbn' => '9780241392362',
                            'qty' => 1,
                            'rowid' => '45c48cce2e2d7fbdea1afc51c7c6ad26',
                            'subtotal' => 260
                        )
                
                );
        $this->unit->run($this->my_cal_discount_lib->cal_discount($arr) , 'is_int', 'Test_cal_culate_discount');
        $this->unit->run($this->my_cal_discount_lib->cal_discount(array()) , 'is_int', 'Test_cal_culate_discount');

        echo $this->unit->report();
    }
  
}

