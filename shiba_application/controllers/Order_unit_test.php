<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_unit_test extends CI_Controller 
{
    public function __construct()
	{
		parent::__construct();
        $this->load->library('unit_test'); 
        $this->load->library('my_cal_discount_lib');
	}
    
    function index()
    {
        $this->load->model('orders_model');
        
        $this->unit->run($this->orders_model->get_orders(), 'is_object', 'Test get all orders');

        $this->unit->run($this->orders_model->get_orders('1'), 'is_object', 'Test get by orders id');

        $this->unit->run($this->orders_model->get_order_detail(), 'is_array', 'Test get all orders and order details');

        $this->unit->run($this->orders_model->get_order_detail('1'), 'is_array', 'Test get orders and order details by order id');

        $this->unit->run($this->orders_model->get_order_detail('0'), 'is_array', 'Test get orders and order details by order id');

        $this->unit->run($this->orders_model->insert_order(array()), 'is_bool', 'Test insert orders');

        $ordData = array(
			'grand_total' => 1240,
			'discount' => 70,
			'cash' =>  1500,
			'order_date' => date("Y-m-d H:i:s"),
			'status' => 1,
			'created_by' => 1
        );
        $this->unit->run($insertOrder =$this->orders_model->insert_order($ordData), 'is_int', 'Test insert orders');

        $this->unit->run($this->orders_model->insert_orders_detail(), 'is_bool', 'Test insert orders');
        // $insertOrder = 5;
        $ordItemData = Array
        (
            0 => Array
                (
                    'orders_id' => $insertOrder,
                    'product_id' => 1,
                    'quantity' => 2 ,
                    'price' => 350,
                    'sub_total' => 700,
                    'status' => 1,
                    'created_by' => 1
                ),
        
            1 => Array
                (
                    'orders_id' => $insertOrder,
                    'product_id' => 2,
                    'quantity' => 1 ,
                    'price' => 350,
                    'sub_total' => 350,
                    'status' => 1,
                    'created_by' => 1
                ),
        
            2 => Array
                (
                    'orders_id' => $insertOrder,
                    'product_id' => 9,
                    'quantity' => 1 ,
                    'price' => 260,
                    'sub_total' => 260,
                    'status' => 1,
                    'created_by' => 1
                )
        
        );
        
        $this->unit->run($this->orders_model->insert_orders_detail($ordItemData), 'is_bool', 'Test insert orders');

        echo $this->unit->report();
    }
  
}

