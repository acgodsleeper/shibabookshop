<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		// Load form library & helper
        $this->load->library('form_validation');
		$this->load->helper('form');

		$this->load->model('product_model');
		$this->load->library('my_cal_discount_lib');
	}

	public function index()
	{
		$data['product'] = $this->product_model->get_product();
		$data['title'] = 'Product';

		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('product/index', $data);
		$this->load->view('templates/footer');
	}

	public function add_to_cart()
	{ 
		$data = array(
			'id' => $this->input->post('product_id'), 
			'name' => $this->input->post('product_name'), 
			'price' => $this->input->post('product_price'), 
			'isbn' => $this->input->post('product_isbn'), 
			'qty' => $this->input->post('quantity'), 
		);
		$this->cart->insert($data);
		echo $this->show_cart(); 
	}

	public function show_cart()
	{ 
		$output = '';
		$no = 0;
		$discount = 0;
		//$this->load->library('MY_cal_discount_lib');
		foreach ($this->cart->contents() as $items) {
			$no++;
			$output .='
				<tr>
					<td>'.$items['name'].'</td>
					<td>'.number_format($items['price']).'</td>
					<td>'.$items['qty'].'</td>
					<td>'.number_format($items['subtotal']).'</td>
					<td><button type="button" id="'.$items['rowid'].'" class="romove_cart btn btn-danger btn-sm">Cancel</button></td>
				</tr>
			';
		}
		
		$discount = $this->my_cal_discount_lib->cal_discount($this->cart->contents());
		$discount_input = array(
			'type'  => 'hidden',
			'name'  => 'discount',
			'id'    => 'discount',
			'value' => $discount,
			'class' => 'discount'
		);
		$output .= '
			<tr>
				<th colspan="3">Discount</th>
				<th colspan="2">'.number_format($discount).form_input($discount_input).' Baht</th>
			</tr>
		';
		$output .= '
			<tr>
				<th colspan="3">Total</th>
				<th colspan="2">'.number_format($this->cart->total()-$discount).' Baht</th>
			</tr>
		';
		return $output;
	}

	public function load_cart()
	{ 
		echo $this->show_cart();
	}

	public function delete_cart()
	{ 
		$data = array(
			'rowid' => $this->input->post('row_id'), 
			'qty' => 0, 
		);
		$this->cart->update($data);
		echo $this->show_cart();
	}

}
