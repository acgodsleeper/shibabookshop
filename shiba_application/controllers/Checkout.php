<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		// Load form library & helper
        $this->load->library('form_validation');
		$this->load->helper('form');
		
        $this->load->model('product_model');
        $this->load->model('orders_model');
		$this->load->library('my_cal_discount_lib');
	}
    
    public function index()
	{
		if($this->cart->total_items() <= 0){
            redirect('product/');
		}
        $cash = $this->input->post('cash');
        $discount = $this->input->post('discount');
        if(isset($cash))
        {
            $this->form_validation->set_rules('cash', 'Cash', 'callback_cash_check');
            if($this->form_validation->run() == true)   //$cash > $this->cart->total()
            {
                $data['cash'] = $cash;
                $data['discount'] = $discount;
                // Insert order
                $order = $this->placeOrder($data);
                
                // If the order submission is successful
                if($order){
                    $this->session->set_userdata('success_msg', 'Order placed successfully.');
                    redirect('checkout/orderSuccess/'.$order);
                }else{
                    $data['error_msg'] = 'Order submission failed, please try again.';
                }
            }
            // else
            // {
            //     //echo $this->form_validation->run();
            //     //echo  form_error('cash','<p class="help-block error">','</p>');exit();
            //     redirect('product/');
            //     $data['error_msg'] = 'Please input cash received.';
            // }
        }
        
        $data['product'] = $this->product_model->get_product();
		$data['title'] = 'Product';

		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('checkout/index', $data);
		$this->load->view('templates/footer');
    }

    public function cash_check($cash)
    {
        if ($cash < ($this->cart->total()-$this->input->post('discount')))
        {
            $this->form_validation->set_message('cash_check', 'Please check cash received.');//.$cash.' < '.$this->cart->total() .'.'
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    public function placeOrder($data)
    {
        // Insert order data
        $ordData = array(
			'grand_total' => $this->cart->total()-$data['discount'],
			'discount' => $data['discount'],
			'cash' =>  $data['cash'],
			'order_date' => date("Y-m-d H:i:s"),
			'status' => 1,
			'created_by' => 1
        );
        $insertOrder = $this->orders_model->insert_order($ordData);
        
        if($insertOrder){
            // Retrieve cart data from the session
            $cartItems = $this->cart->contents();
            
            // Cart items
            $ordItemData = array();
            $i=0;
            foreach($cartItems as $item){
                $ordItemData[$i]['orders_id']   = $insertOrder;
                $ordItemData[$i]['product_id']  = $item['id'];
                $ordItemData[$i]['quantity']    = $item['qty'];
                $ordItemData[$i]['price']       = $item["price"];
                $ordItemData[$i]['sub_total']   = $item["subtotal"];
                $ordItemData[$i]['status']      = 1;
                $ordItemData[$i]['created_by']  = 1;
                $i++;
            }
            
            if(!empty($ordItemData)){
                // Insert order items
                $insertOrderItems = $this->orders_model->insert_orders_detail($ordItemData);
                
                if($insertOrderItems){
                    // Remove items from the cart
                    $this->cart->destroy();
                    
                    // Return order ID
                    return $insertOrder;
                }
            }
        }
        return false;
    }

    public function orderSuccess($ordID)
    {
        // Fetch order data from the database
        $data['order'] = $this->orders_model->get_order_detail($ordID);
        $data['product'] = $this->product_model->get_product();
        
        // Load order details view
        $this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('checkout/order_success', $data);
		$this->load->view('templates/footer');
        
    }

    public function receipt($ordID)
    {
        // Fetch order data from the database
        $data['order'] = $this->orders_model->get_order_detail($ordID);
        $data['product'] = $this->product_model->get_product();
        
        // Load order details view
        //$this->load->view('templates/header', $data);
		//$this->load->view('templates/sidebar', $data);
		$this->load->view('checkout/receipt', $data);
		//$this->load->view('templates/footer');
        
    }
}