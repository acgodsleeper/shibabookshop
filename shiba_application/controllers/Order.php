<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('orders_model');
		//$this->load->helper('url_helper');
	}

	public function index()
	{
		$data['order'] = $this->orders_model->get_orders();
		$data['title'] = 'Order';

		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('order/index', $data);
		$this->load->view('templates/footer');
	}

}
