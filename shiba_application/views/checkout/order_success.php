<div class="content p-4">
    <h2 class="mb-4">Shopping Cart</h2>

    <div class="col-md-9">
        <!-- <h4>Shopping Cart </h4> -->
        <div class="col-md-2" style="float: right !important;">
            <span><a href="<?php echo base_url();?>product"><img src="<?php echo base_url();?>_resources/images/buy_logo1.png" title="Go To Product" width="28"></a> <a href="<?php echo base_url();?>checkout/receipt/<?php echo $order['id'];?>" target="_blank"><img src="<?php echo base_url();?>_resources/images/receipt.png" title="Print Receipt" width="28"></a></span> 
        </div>
        <form class="form-horizontal" action="<?php echo base_url();?>checkout" method="post">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Cover</th>
                    <th>Items</th>
                    <th>Price</th>
                    <th>Qty</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody id="detail_cart5">
                <?php foreach($order['items'] as $item) : ?>
                    <tr>
                        <td><img src="<?php echo $item['cover'];?>" width="50"/></td>
                        <td><?php echo $item['title'];?></td>
                        <td><?php echo number_format($item['price']);?></td>
                        <td><?php echo $item['quantity'];?></td>
                        <td><?php echo number_format($item['sub_total']);?></td>
                    </tr>
                <?php endforeach;?>
                <tr>
                    <th colspan="4">Discount</th>
                    <th colspan="1"><?php echo number_format($order['discount']);?> Baht</td>
                </tr>
                <tr>
                    <th colspan="2">Total</th>
                    <th colspan="3"><?php echo number_format($order['grand_total']);?> Baht</th>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="2">Cash/Change</th>
                    <th colspan="2"><?php echo number_format($order['cash']);?> Baht</th>
                    <th colspan="1"><?php echo number_format($order['cash']-$order['grand_total']);?> Baht</th>
                </tr>
            </tfoot>
            
        </table>
        </form>
    </div>
</div>