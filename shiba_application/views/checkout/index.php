
<div class="content p-4">
        <h2 class="mb-4">Shopping Cart</h2>

        <div class="col-md-9">
			<h4>Shopping Cart</h4>
			<form class="form-horizontal" action="<?php echo base_url();?>checkout" method="post">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Items</th>
						<th>Price</th>
						<th>Qty</th>
						<th>Total</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody id="detail_cart">

				</tbody>
				<tfoot>
					<tr>
						<th>Cash</th>
						<td colspan="2"><?php echo  form_error('cash','<p class="text-danger">','</p>');?></td>
						<td> <input type="number" name="cash" id="cash" aling="right" value="0" class="quantity form-control"></td>
						<td><button type="submit" name="checkOut" class="btn btn-success orderBtn">Check Out <i class="glyphicon glyphicon-menu-right"></i></button></td>
					</tr>
				</tfoot>
				
			</table>
			</form>
		</div>
		<table id="dataTable" class="display" style="width:100%"> 
			<thead> 
				<tr> <th>Cover</th> <th>Name</th><th>ISBN</th> <th>Price</th><th></th><th>Add to Cart</th> </tr> 
			</thead> 
			<tbody> 
				<?php foreach($product->result() as $row_product) :?>
				<tr>
					<td><img src="<?php echo $row_product->cover;?>"/></td>
					<td><?php echo $row_product->title;?></td>
					<td><?php echo $row_product->isbn;?></td>
					<td><?php echo $row_product->price;?></td>
					<td><input type="number" name="quantity" id="<?php echo $row_product->id;?>" value="1" class="quantity form-control"></td>
					<td>
					<button class="add_cart btn btn-success btn-block" data-productid="<?php echo $row_product->id;?>" data-productname="<?php echo str_replace("'", '',preg_replace('/[^a-zA-Z0-9\']/', ' ',$row_product->title));?>" data-productprice="<?php echo $row_product->price;?>" data-productisbn="<?php echo $row_product->isbn;?>">Add To Cart</button>
					<!--<a href='".base_url()."product/add_to_cart/".$row_product->id."'><img src='".base_url()."_resources/images/buy_logo1.png' width='40'/></a>-->

					</td>
				</tr>

				<?php endforeach;?>
			</tbody> 
			<tfoot> 
				<tr> <th>Cover</th> <th>Name</th><th>ISBN</th> <th>Price</th><th></th><th>Add to Cart</th></tr> 
			</tfoot> 
		</table>
    </div>