<table class="table table-striped" width="400">
    <thead>
        <tr>
            <th colspan="4">Shiba Book Shop</th>
        </tr>
        <tr>
            <th colspan="4">TAX#1234567890123 (Vat Included)</th>
        </tr>
        <tr>
            <th colspan="4">POS# : SB<?php echo str_pad($order['id'], 10, '0', STR_PAD_LEFT);?></th>
        </tr>
        <tr><td colspan="4">&nbsp;</td></tr>
        <tr>
            <th colspan="4">ใบเสร็จรับเงิน/ใบกำกับภาษีอย่างย่อ</th>
        </tr>
    </thead>
    <tbody id="detail_cart5">
        <?php foreach($order['items'] as $item) : ?>
            <tr>
                <td align="center"><?php echo $item['quantity'];?>&nbsp;</td>
                <td><?php echo $item['title'];?></td>
                <td>&nbsp;</td>
                <td><?php echo number_format($item['sub_total']);?></td>
            </tr>
        <?php endforeach;?>
        <tr><td colspan="4">&nbsp;</td></tr>
        <tr><td colspan="4">&nbsp;</td></tr>
        <tr>
            <th colspan="3" align="left">Discount</th>
            <th colspan="1"><?php echo number_format($order['discount']);?> Baht</td>
        </tr>
        <tr>
            <th colspan="1" align="left">Total</th>
            <th colspan="2"><?php echo number_format($order['grand_total']-$order['discount']);?> Baht</th>
            <th colspan="1">&nbsp;</th>
        </tr>
    </tbody>
    <tfoot>
        <tr>
            <th colspan="1" align="left">Cash/Change</th>
            <th colspan="2"><?php echo number_format($order['cash']);?> Baht</th>
            <th colspan="1"><?php echo number_format($order['cash']-$order['grand_total']+$order['discount']);?> Baht</th>
        </tr>
        <tr><td colspan="4">&nbsp;</td></tr>
        <tr><td colspan="4">&nbsp;</td></tr>
        <tr><td colspan="4" align="center"><?php $date=date_create($order['order_date']); echo date_format($date,"d/m/Y H:i:s");?></td></tr>
    </tfoot>
</table>