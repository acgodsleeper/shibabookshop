
<div class="content p-4">
        <h2 class="mb-4">Order</h2>

		<table id="dataTable" class="display" style="width:100%"> 
			<thead> 
				<tr> <th>ID</th> <th>Order Date</th><th>Grand Total</th> <th>Discount</th><th>Cash</th> <th>Receipt</th> </tr> 
			</thead> 
			<tbody> 
				<?php
					if(!empty($order)){
						foreach($order->result() as $row_order)
						{
							echo '<tr>
									<td>'.$row_order->id.'</td>
									<td>'.$row_order->order_date.'</td>
									<td>'.number_format($row_order->grand_total).'</td>
									<td>'.number_format($row_order->discount).'</td>
									<td>'.number_format($row_order->cash).'</td>
									<td><a href="'.base_url().'checkout/receipt/'.$row_order->id.'" target="_blank"><img src="'.base_url().'_resources/images/receipt.png" title="Print Receipt" width="28"></a></td>
								</tr>';
						}
					}
				?>
			</tbody> 
			<tfoot> 
				<tr> <th>ID</th> <th>Order Date</th><th>Grand Total</th> <th>Discount</th><th>Cash</th><th>Receipt</th></tr> 
			</tfoot> 
		</table>
    </div>