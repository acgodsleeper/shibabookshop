<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
</div>

<script src="<?php echo base_url();?>_resources/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>_resources/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url();?>_resources/js/bootadmin.min.js"></script>
<script type="text/javascript" charset="utf8" src="<?php echo base_url();?>_resources/js/datatables.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#dataTable').DataTable();
		} );
	</script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.add_cart').click(function(){
				var product_id    = $(this).data("productid");
				var product_name  = $(this).data("productname");//.replace("'","&#39;");
				var product_price = $(this).data("productprice");
				var product_isbn = $(this).data("productisbn");
				var quantity   	  = $('#' + product_id).val();
				//alert(product_id+product_name);
				$.ajax({
					url : "<?php echo site_url('product/add_to_cart');?>",
					method : "POST",
					data : {product_id: product_id, product_name: product_name, product_price: product_price, product_isbn: product_isbn, quantity: quantity},
					success: function(data){
						$('#detail_cart').html(data);
					}
				});
			});

			
			$('#detail_cart').load("<?php echo site_url('product/load_cart');?>");

			
			$(document).on('click','.romove_cart',function(){
				var row_id=$(this).attr("id"); 
				$.ajax({
					url : "<?php echo site_url('product/delete_cart');?>",
					method : "POST",
					data : {row_id : row_id},
					success :function(data){
						$('#detail_cart').html(data);
					}
				});
			});

			
		});
	</script>
	
</body>
</html>