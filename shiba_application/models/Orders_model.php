<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Orders_model extends CI_Model
{
    public function get_orders($id=null)
    {
        if(isset($id))
        {
            $this->db->where('id',$id);
        }
        return $this->db->get('orders');
    }

    public function get_order_detail($id=null)
    {
        if(isset($id))
        {
            $this->db->where('id',$id);
        }
        //$query = $this->db->get('orders');
        $result = $this->db->get('orders')->row_array();
        
        // Get order items
        $this->db->select('ord_dt.*, prd.cover, prd.title, prd.price, prd.isbn');
        $this->db->from('orders_detail'.' as ord_dt');
        $this->db->join('product as prd', 'prd.id = ord_dt.product_id', 'left');
        $this->db->where('ord_dt.orders_id', $id);
        $query = $this->db->get();//echo $this->db->last_query();
        $result['items'] = ($query->num_rows() > 0)?$query->result_array():array();
        
        // Return fetched data
        return !empty($result)?$result:false;
    }
    
    public function insert_order($data)
    {
        if(!empty($data))
        {
            $query = $this->db->insert('orders', $data);
            if ($query)
                return $this->db->insert_id();
            else
                return FALSE;
        }
        else
            return FALSE;
    }

    public function insert_orders_detail($data = array()) {
        
        if(!empty($data))
        {
            // Insert order items
            $insert = $this->db->insert_batch('orders_detail', $data);
        }
        else
            return false;

        // Return the status
        return $insert?true:false;
    }
}