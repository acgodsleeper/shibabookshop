<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product_model extends CI_Model
{
    public function get_product($id=null)
    {
        if(isset($id))
        {
            $this->db->where('id',$id);
        }
        return $this->db->get('product');
    }

    public function get_product_by_category_id($category_id=null)
    {
        if(isset($category_id))
        {
            $this->db->where('category_id',$category_id);
        }
        return $this->db->get('product');
    }
    
}