How to setup configuration for local development

1.Clone project from link : https://gitlab.com/acgodsleeper/shibabookshop.git 

2.If you don't have XAMPP please download and install XAMPP support PHP version 7.3.0+ 

3.Copy this project to folder htdocs and change name folder project to "Shiba_Bookshop"

4.Setup ENVIRONTMENT in file "Shiba_Bookshop\index.php" and setup to development for local and setup production for server
	- define('ENVIRONMENT', isset($_SERVER['CI_ENV']) ? $_SERVER['CI_ENV'] : 'development');
	
5.Create Database name "shiba_bookshop" in phpmyadmin and import "shiba_bookshop.sql" to create table and insert data to table product

6.Setup configuration to connect database in file "Shiba_Bookshop\shiba_application\config\database.php" 

$db['default'] = array(
	'dsn'	=> '',
	'hostname' => 'localhost',
	'username' => 'root',
	'password' => 'YOUR-PASSWORD-DATABASE',
	'database' => 'shiba_bookshop',
	
7.Run URL : http://localhost/Shiba_Bookshop


Source control Link : https://gitlab.com/acgodsleeper/shibabookshop
