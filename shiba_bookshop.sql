-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 17, 2019 at 10:33 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shiba_bookshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `status`) VALUES
(1, 'Harry Potter', 1),
(2, 'Others', 1);

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `role_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `name`, `role_id`, `status`, `created_by`, `created_on`, `updated_by`, `updated_on`) VALUES
(1, 'Shiba Super Admin Shop', 1, 1, 1, '2019-03-14 15:07:26', NULL, NULL),
(2, 'Shiba Admin Shop', 2, 1, 1, '2019-03-14 15:07:26', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employee_role`
--

CREATE TABLE `employee_role` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employee_role`
--

INSERT INTO `employee_role` (`id`, `name`, `status`, `created_by`, `created_on`, `updated_by`, `updated_on`) VALUES
(1, 'Super Admin', 1, 1, '2019-03-14 15:03:37', NULL, NULL),
(2, 'Admin', 1, 1, '2019-03-14 15:03:37', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `grand_total` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `cash` int(11) NOT NULL,
  `order_date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `orders_detail`
--

CREATE TABLE `orders_detail` (
  `id` int(11) NOT NULL,
  `orders_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `sub_total` float NOT NULL,
  `status` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `cover` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `isbn` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `category_id`, `title`, `cover`, `price`, `isbn`, `status`, `created_by`, `created_on`, `updated_by`, `updated_on`) VALUES
(1, 1, 'Harry Potter and the Philosopher\'s Stone (I)', 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9781/4088/9781408855652.jpg', 350, '9781408855652', 1, 1, '2012-03-19 01:41:00', NULL, NULL),
(2, 1, 'Harry Potter and the Chamber of Secrets (II)', 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9781/4088/9781408855669.jpg', 350, '9781408855669', 1, 1, '2012-03-19 01:41:00', NULL, NULL),
(3, 1, 'Harry Potter and the Prisoner of Azkaban (III)', 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9781/4088/9781408855676.jpg', 340, '9781408855676', 1, 1, '2012-03-19 01:41:00', NULL, NULL),
(4, 1, 'Harry Potter and the Goblet of Fire (IV)', 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9781/4088/9781408855683.jpg', 360, '9781408855683', 1, 1, '2012-03-19 01:41:00', NULL, NULL),
(5, 1, 'Harry Potter and the Order of the Phoenix (V)', 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9781/4088/9781408855690.jpg', 380, '9781408855690', 1, 1, '2012-03-19 01:41:00', NULL, NULL),
(6, 1, 'Harry Potter and the Half-Blood Prince (VI)', 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9781/4088/9781408855706.jpg', 380, '9781408855706', 1, 1, '2012-03-19 01:41:00', NULL, NULL),
(7, 1, 'Harry Potter and the Deathly Hallows (VII)', 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9781/4088/9781408855713.jpg', 400, '9781408855713', 1, 1, '2012-03-19 01:41:00', NULL, NULL),
(8, 2, 'The Order of the Day', 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9781/5098/9781509889969.jpg', 270, '9781509889969', 1, 1, '2012-03-19 01:41:00', NULL, NULL),
(9, 2, 'The Fork, the Witch, and the Worm', 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9780/2413/9780241392362.jpg', 260, '9780241392362', 1, 1, '2012-03-19 01:41:00', NULL, NULL),
(10, 2, 'The Dakota Winters', 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9781/4711/9781471128387.jpg', 220, '9781471128387', 1, 1, '2012-03-19 01:41:00', NULL, NULL),
(11, 2, 'Mistborn: Secret History', 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9781/4732/9781473225046.jpg', 320, '9781473225046', 1, 1, '2012-03-19 01:41:00', NULL, NULL),
(12, 2, 'Professor Chandra Follows His Bliss', 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9781/7847/9781784742539.jpg', 280, '9781784742539', 1, 1, '2012-03-19 01:41:00', NULL, NULL),
(13, 2, 'The Rosie Result', 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9781/9257/9781925773477.jpg', 180, '9781925773477', 1, 1, '2012-03-19 01:41:00', NULL, NULL),
(14, 2, 'Come Rain or Come Shine', 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9780/5713/9780571351749.jpg', 275, '9780571351749', 1, 1, '2012-03-19 01:41:00', NULL, NULL),
(15, 2, 'The Illumination of Ursula Flight', 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9781/7606/9781760632021.jpg', 310, '9781760632021', 1, 1, '2012-03-19 01:41:00', NULL, NULL),
(16, 2, 'A Shout in the Ruins', 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9781/4736/9781473667815.jpg', 290, '9781473667815', 1, 1, '2012-03-19 01:41:00', NULL, NULL),
(17, 2, 'The Ocean Book', 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9780/8578/9780857844774.jpg', 260, '9780857844774', 1, 1, '2012-03-19 01:41:00', NULL, NULL),
(18, 2, 'Enlightenment Now', 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9780/1419/9780141979090.jpg', 220, '9780141979090', 1, 1, '2012-03-19 01:41:00', NULL, NULL),
(19, 2, 'The New Complete Book of Self-Sufficiency', 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9780/2413/9780241352465.jpg', 190, '9780241352465', 1, 1, '2012-03-19 01:41:00', NULL, NULL),
(20, 2, 'The Monk of Mokha', 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9780/2419/9780241975367.jpg', 230, '9780241975367', 1, 1, '2012-03-19 01:41:00', NULL, NULL),
(21, 2, 'The Language of Kindness', 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9781/7847/9781784706883.jpg', 300, '9781784706883', 1, 1, '2012-03-19 01:41:00', NULL, NULL),
(22, 2, 'Elastic', 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9780/1419/9780141987392.jpg', 250, '9780141987392', 1, 1, '2012-03-19 01:41:00', NULL, NULL),
(23, 2, 'Death Row: The Final Minutes', 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9781/7887/9781788701495.jpg', 230, '9781788701495', 1, 1, '2012-03-19 01:41:00', NULL, NULL),
(24, 2, 'Dog Man 4: Dog Man and Cat Kid', 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9781/4071/9781407192123.jpg', 310, '9781407192123', 1, 1, '2012-03-19 01:41:00', NULL, NULL),
(25, 2, 'The Colour of the Sun', 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9781/4449/9781444941135.jpg', 180, '9781444941135', 1, 1, '2012-03-19 01:41:00', NULL, NULL),
(26, 2, 'The Minimalist Home: A Room-By-Room Guide to a Decluttered, Refocused Life', 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9781/6014/9781601427991.jpg', 400, '9781601427991', 1, 1, '2012-03-19 01:41:00', NULL, NULL),
(27, 2, 'How to Survive the End of the World ', 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9781/4736/9781473659711.jpg', 120, '9781473659711', 1, 1, '2012-03-19 01:41:00', NULL, NULL),
(28, 2, 'Solve For Happy', 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9781/5098/9781509809950.jpg', 160, '9781509809950', 1, 1, '2012-03-19 01:41:00', NULL, NULL),
(29, 2, 'The Confidence Project', 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9781/4736/9781473634176.jpg', 345, '9781473634176', 1, 1, '2012-03-19 01:41:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_detail`
--

CREATE TABLE `product_detail` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_emp_role` (`role_id`);

--
-- Indexes for table `employee_role`
--
ALTER TABLE `employee_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders_detail`
--
ALTER TABLE `orders_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_detail`
--
ALTER TABLE `product_detail`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `employee_role`
--
ALTER TABLE `employee_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders_detail`
--
ALTER TABLE `orders_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `product_detail`
--
ALTER TABLE `product_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `employee`
--
ALTER TABLE `employee`
  ADD CONSTRAINT `FK_emp_role` FOREIGN KEY (`role_id`) REFERENCES `employee_role` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
